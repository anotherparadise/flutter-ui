import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui/app/theme.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flexible Pikachu',
      theme: appTheme,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
                flex: 5,
                fit: FlexFit.loose,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Flexible(
                        flex: 1,
                        child: Container(
                          color: Colors.green[100],
                          child: Column(
                            children: [
                              Flexible(
                                  flex: 2,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    child: Container(
                                        child: Image.asset('images/fire.png')),
                                  )),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  'Pikachu',
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Text(
                                  'Community we all need',
                                  style: GoogleFonts.jura(
                                      textStyle: TextStyle(
                                          color: Colors.red,
                                          fontWeight: FontWeight.w200)),
                                ),
                              ),
                            ],
                          ),
                        )),
                    Flexible(
                        flex: 2,
                        child: Container(
                          color: Colors.amber,
                          child: Padding(
                              padding: EdgeInsets.all(50),
                              child: Container(
                                child: ColorFiltered(
                                    colorFilter: ColorFilter.mode(
                                        Colors.amber, BlendMode.modulate),
                                    child: Image.asset('images/pikachu.png')),
                              )),
                        ))
                  ],
                )),
            Flexible(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Flexible(
                        flex: 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              'How are you?',
                              style: GoogleFonts.aclonica(
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Ni Hao',
                              style: GoogleFonts.aclonica(
                                  fontSize: 10, color: Colors.grey),
                            ),
                            Text(
                              'Bienvenue',
                              style: GoogleFonts.aclonica(
                                  fontSize: 10, color: Colors.grey),
                            )
                          ],
                        )),
                    Flexible(
                        flex: 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 150,
                              height: 50,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.9)),
                                onPressed: () => {},
                                child: Text(
                                  'Sign Up',
                                  style: GoogleFonts.jura(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            SizedBox(
                              width: 150,
                              height: 50,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.9)),
                                onPressed: () => {},
                                child: Text('Log In',
                                    style: GoogleFonts.jura(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ],
                        ))
                  ],
                )),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
