import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_ui/app/theme.dart';
import 'package:flutter_ui/e-commerce/detail.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Video Chat',
    theme: appTheme,
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.menu_rounded,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            children: [
              Flexible(
                flex: 1,
                child: Container(
                  // color: Colors.purple,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Flexible(
                        flex: 15,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white,
                              shape: BoxShape.rectangle,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black.withAlpha(20),
                                    blurRadius: 20.0,
                                    spreadRadius: 6.0)
                              ]),
                          child: Column(
                            children: [
                              Expanded(
                                flex: 4,
                                child: Column(
                                  children: [
                                    Spacer(
                                      flex: 1,
                                    ),
                                    Expanded(
                                        flex: 5,
                                        child: ClipOval(
                                            child: Image.asset(
                                                'images/model.jpg'))),
                                    Spacer(
                                      flex: 1,
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Column(
                                        children: [
                                          Expanded(
                                              child: Text(
                                            'Mona White',
                                            style: GoogleFonts.aBeeZee(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20),
                                          )),
                                          Expanded(
                                              child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                Container(
                                                  color: Colors.greenAccent,
                                                  height: 10,
                                                  width: 10,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text('Online'),
                                              ])),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Spacer(
                                      flex: 1,
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: Padding(
                                        padding:
                                            EdgeInsets.only(top: 5, bottom: 10),
                                        child: Container(
                                          height: double.infinity,
                                          child: RaisedButton(
                                            color: Colors.white,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            onPressed: () {},
                                            child: Text(
                                              'Message Her',
                                              style: GoogleFonts.aBeeZee(
                                                  color: Colors.black),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Spacer(
                                      flex: 1,
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Spacer(
                                      flex: 1,
                                    ),
                                    Expanded(
                                      flex: 8,
                                      child: Padding(
                                        padding:
                                            EdgeInsets.only(top: 5, bottom: 10),
                                        child: Container(
                                          height: double.infinity,
                                          child: RaisedButton(
                                            color: Colors.blue,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            onPressed: () {},
                                            child: Text(
                                              'Start Video Call',
                                              style: GoogleFonts.aBeeZee(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Spacer(
                                      flex: 1,
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Flexible(
                        flex: 10,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white,
                              shape: BoxShape.rectangle,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black.withAlpha(20),
                                    blurRadius: 20.0,
                                    spreadRadius: 6.0)
                              ]),
                          child: Padding(
                            padding:
                                EdgeInsets.only(left: 10, right: 10, top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Flexible(
                                    flex: 1,
                                    child: Text(
                                      'Scheduled events',
                                      style: GoogleFonts.lato(
                                          fontSize: 18,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    )),
                                Flexible(
                                  fit: FlexFit.tight,
                                  flex: 3,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                color: Colors.purple),
                                            height: 80,
                                            width: 80,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '01',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 20),
                                                  ),
                                                  Text('November',
                                                      style: TextStyle(
                                                          color: Colors.white))
                                                ],
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'American Top Model',
                                                style: GoogleFonts.lato(
                                                  fontSize: 16,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text(
                                                '10:30 pm',
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                      Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.grey
                                                        .withAlpha(60),
                                                    blurRadius: 2,
                                                    spreadRadius: 2.0)
                                              ]),
                                          child: Padding(
                                            padding: EdgeInsets.all(10),
                                            child: Icon(
                                              Icons.phone,
                                              color: Colors.purple,
                                            ),
                                          ))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Spacer(
                        flex: 1,
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Flexible(
                        flex: 1,
                        child: ClipOval(
                          child: Container(
                            color: Colors.grey[100],
                            width: 50,
                            height: 50,
                            child: Icon(Icons.lightbulb, color: Colors.grey),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: ClipOval(
                          child: Container(
                            color: Colors.grey[100],
                            width: 50,
                            height: 50,
                            child: Icon(Icons.video_call, color: Colors.grey),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: ClipOval(
                          child: Container(
                            color: Colors.grey[100],
                            width: 50,
                            height: 50,
                            child: Icon(Icons.power, color: Colors.grey),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: ClipOval(
                          child: Container(
                            color: Colors.grey[100],
                            width: 50,
                            height: 50,
                            child: Icon(Icons.audiotrack, color: Colors.grey),
                          ),
                        ),
                      )
                    ],
                  ),
                  height: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(18),
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withAlpha(20),
                            blurRadius: 20.0,
                            spreadRadius: 6.0)
                      ])),
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
