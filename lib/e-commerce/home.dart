import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_ui/app/theme.dart';
import 'package:flutter_ui/e-commerce/detail.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'PC computer store',
    theme: appTheme,
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Colors.black12,
        backgroundColor: Colors.white24,
        // type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: [
          BottomNavigationBarItem(
              activeIcon: Icon(
                Icons.home,
                color: Colors.black87,
              ),
              label: 'Home',
              icon: Icon(
                Icons.home,
                color: Colors.black12,
              )),
          BottomNavigationBarItem(
              label: 'Chat',
              icon: Icon(
                Icons.chat,
                color: Colors.black38,
              )),
          BottomNavigationBarItem(
              label: 'Bag',
              icon: Icon(
                Icons.shopping_bag,
                color: Colors.black38,
              )),
          BottomNavigationBarItem(
              label: 'Notifications',
              icon: Icon(
                Icons.notifications,
                color: Colors.black38,
              )),
          BottomNavigationBarItem(
              label: 'Account',
              icon: Icon(
                Icons.account_circle,
                color: Colors.black38,
              )),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Flexible(
                          //https://fluttercorner.com/flutter-renderbox-was-not-laid-out/  Flutter: RenderBox was not laid out
                          flex: 17,
                          child: Material(
                            elevation: 5,
                            borderRadius: BorderRadius.circular(6),
                            child: TextFormField(
                              autofocus: false,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.search,
                                    color: Colors.black,
                                  ),
                                  disabledBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  hintText: 'Search your favorite items',
                                  hintStyle: TextStyle(color: Colors.grey)),
                            ),
                          ),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        Flexible(
                          flex: 2,
                          child: AspectRatio(
                            aspectRatio: 1 / 1,
                            child: Material(
                                elevation: 10,
                                borderRadius: BorderRadius.circular(6),
                                color: Colors.grey,
                                child: IconButton(
                                    icon: Icon(
                                      Icons.shopping_cart,
                                      color: Colors.white,
                                    ),
                                    onPressed: () => {})),
                          ),
                        )
                      ],
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.topCenter,
                        // color: Colors.orange,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            AspectRatio(
                                aspectRatio: 16 / 5,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset(
                                    'images/amdryzen.jpg',
                                    fit: BoxFit.fitWidth,
                                  ),
                                )),
                            Text(
                              'AMD RYZEN',
                              style: GoogleFonts.lato(
                                  textStyle: TextStyle(
                                      color: Colors.black54,
                                      fontWeight: FontWeight.w900)),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Flexible(
                        flex: 5,
                        child: Container(
                            alignment: Alignment.topLeft,
                            child: Text(
                              'Catalog',
                              style: GoogleFonts.lato(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ))),
                    Spacer(
                      flex: 2,
                    ),
                    Expanded(
                      flex: 36,
                      child: Container(
                        child: Card(
                          elevation: 6,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Column(
                            children: [
                              Flexible(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: FlatButton(
                                        onPressed: () => {},
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Image.asset('images/mac.png'),
                                            Text('mac')
                                          ],
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: () => {},
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Image.asset('images/pc.png'),
                                              Text('pc')
                                            ],
                                          ),
                                        )),
                                    Flexible(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: () => {},
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Image.asset('images/mouse.png'),
                                              Text('mouse')
                                            ],
                                          ),
                                        )),
                                    Flexible(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: () => {},
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Image.asset(
                                                  'images/keyboard.png'),
                                              Text(
                                                'keyboard',
                                                overflow: TextOverflow.ellipsis,
                                              )
                                            ],
                                          ),
                                        ))
                                  ],
                                ),
                              ),
                              Flexible(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Flexible(
                                      flex: 1,
                                      child: FlatButton(
                                        onPressed: () => {},
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Image.asset('images/tablet.png'),
                                            Text('tablet')
                                          ],
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: () => {},
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Image.asset(
                                                  'images/playstation.png'),
                                              Text('P.S')
                                            ],
                                          ),
                                        )),
                                    Flexible(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: () => {},
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Image.asset('images/xbox.png'),
                                              Text('xbox')
                                            ],
                                          ),
                                        )),
                                    Flexible(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: () => {},
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Image.asset('images/switch.png'),
                                              Text('switch')
                                            ],
                                          ),
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: [
                    Flexible(
                      child: Container(
                          alignment: Alignment.topLeft,
                          child: Container(
                              alignment: Alignment.topLeft,
                              child: Padding(
                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                child: Text(
                                  'Top pick',
                                  style: GoogleFonts.lato(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18),
                                ),
                              ))),
                      flex: 6,
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    Flexible(
                        flex: 28,
                        child: Row(
                          children: [
                            Flexible(
                              flex: 20,
                              child: MaterialButton(
                                elevation: 10,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15.0),
                                        bottomLeft: Radius.circular(15.0))),
                                padding: EdgeInsets.zero,
                                color: Colors.white,
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Detail()));
                                },
                                child: Column(
                                  children: [
                                    AspectRatio(
                                      aspectRatio: 5 / 3,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(15)),
                                        child: Image.asset(
                                          'images/ryzen-gallery.jpg',
                                          fit: BoxFit.fitHeight,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 6, top: 10, right: 6),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              'AMD RYZEN 7 3800 XT',
                                              style:
                                                  TextStyle(color: Colors.pink),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  '\$749.99',
                                                  style: TextStyle(
                                                      color: Colors.green,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                RatingBarIndicator(
                                                  //https://pub.dev/packages/flutter_rating_bar
                                                  rating: 5,
                                                  itemBuilder:
                                                      (context, index) => Icon(
                                                    Icons.star,
                                                    color: Colors.amber,
                                                  ),
                                                  itemCount: 5,
                                                  itemSize: 10.0,
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Spacer(
                              flex: 1,
                            ),
                            Flexible(
                              flex: 20,
                              child: MaterialButton(
                                elevation: 10,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(15.0),
                                        bottomRight: Radius.circular(15.0))),
                                padding: EdgeInsets.zero,
                                color: Colors.white,
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Detail()));
                                },
                                child: Column(
                                  children: [
                                    AspectRatio(
                                      aspectRatio: 5 / 3,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(15)),
                                        child: Image.asset(
                                          'images/ryzen2-gallery.jpg',
                                          fit: BoxFit.fitHeight,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 6, top: 10, right: 6),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Text(
                                              'AMD RYZEN 7 5800 XT',
                                              style:
                                                  TextStyle(color: Colors.pink),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  '\$999.99',
                                                  style: TextStyle(
                                                      color: Colors.green,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                RatingBarIndicator(
                                                  //https://pub.dev/packages/flutter_rating_bar
                                                  rating: 4,
                                                  itemBuilder:
                                                      (context, index) => Icon(
                                                    Icons.star,
                                                    color: Colors.amber,
                                                  ),
                                                  itemCount: 5,
                                                  itemSize: 10.0,
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ))
                  ],
                ),
              )
            ],
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
