import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ui/app/theme.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(Christmas());
}

class Christmas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: appTheme,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Colors.black12,
        backgroundColor: Colors.white24,
        // type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: [
          BottomNavigationBarItem(
              activeIcon: Icon(
                Icons.home,
                color: Colors.deepPurpleAccent,
              ),
              label: 'Home',
              icon: Icon(
                Icons.home,
                color: Colors.black12,
              )),
          BottomNavigationBarItem(
              label: 'Download',
              icon: Icon(
                Icons.cloud_download_outlined,
                color: Colors.black38,
              )),
          BottomNavigationBarItem(
              label: 'Purchases',
              icon: Icon(
                Icons.menu_rounded,
                color: Colors.black38,
              )),
          BottomNavigationBarItem(
              label: 'Account',
              icon: Icon(
                Icons.account_circle_outlined,
                color: Colors.black38,
              )),
        ],
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text(
          'What\'s new  ❤',
          style: GoogleFonts.lato(
              fontWeight: FontWeight.bold, color: Colors.black),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.menu_outlined),
            color: Colors.deepPurpleAccent,
            onPressed: () {},
          )
        ],
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.notifications_none),
          color: Colors.deepPurpleAccent,
          onPressed: () {},
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(35),
                      child: Image.asset(
                        'images/christmas-2.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                    Column(
                      children: [
                        Spacer(
                          flex: 5,
                        ),
                        Flexible(
                          flex: 3,
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30)),
                            child: Padding(
                              padding: EdgeInsets.only(left: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                        flex: 8,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Christmas Hits 2020',
                                              style: GoogleFonts.adamina(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20),
                                            ),
                                            SizedBox(
                                              height: 6,
                                            ),
                                            Text(
                                              'Bobby Helms, Frank Sinatra, Michael Bublé, Mariah Carey',
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              style: GoogleFonts.adamina(
                                                  color: Colors.black),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 2,
                                        child: Container(
                                          width: 70,
                                          height: 70,
                                          child: FloatingActionButton(
                                            backgroundColor:
                                                Colors.deepPurpleAccent,
                                            onPressed: () {},
                                            child: Icon(
                                              Icons.play_arrow_sharp,
                                              color: Colors.white,
                                              size: 40,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        child: Text(
                          'Trending Sets',
                          style: GoogleFonts.lato(
                              fontWeight: FontWeight.bold, fontSize: 25),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 6,
                    fit: FlexFit.tight,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Card(
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            child: AspectRatio(
                                aspectRatio: 1,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Spacer(
                                        flex: 1,
                                      ),
                                      Flexible(
                                          flex: 10,
                                          child: AspectRatio(
                                            aspectRatio: 1 / 1,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: Image.asset(
                                                'images/christmas-3.jpg',
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          )),
                                      Spacer(
                                        flex: 1,
                                      ),
                                      Flexible(
                                          flex: 2,
                                          child: Text('Joy to the World - 1'))
                                    ])),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Card(
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            child: AspectRatio(
                                aspectRatio: 1,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Spacer(
                                        flex: 1,
                                      ),
                                      Flexible(
                                          flex: 10,
                                          child: AspectRatio(
                                            aspectRatio: 1 / 1,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: Image.asset(
                                                'images/christmas-4.jpg',
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          )),
                                      Spacer(
                                        flex: 1,
                                      ),
                                      Flexible(
                                          flex: 2,
                                          child: Text('Joy to the World - 2'))
                                    ])),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Card(
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            child: AspectRatio(
                                aspectRatio: 1,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Spacer(
                                        flex: 1,
                                      ),
                                      Flexible(
                                          flex: 10,
                                          child: AspectRatio(
                                            aspectRatio: 1 / 1,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: Image.asset(
                                                'images/christmas-5.jpg',
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          )),
                                      Spacer(
                                        flex: 1,
                                      ),
                                      Flexible(
                                          flex: 2,
                                          child: Text('Joy to the World - 3'))
                                    ])),
                          ),
                        )
                      ],
                    ),
                  ),
                  Spacer(
                    flex: 2,
                  )
                ],
              ),
            )
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
