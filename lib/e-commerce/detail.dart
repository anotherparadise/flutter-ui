import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_ui/app/theme.dart';
import 'package:google_fonts/google_fonts.dart';

class Detail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flexible Pikachu',
      theme: appTheme,
      home: Scaffold(
/*        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          leading: Icon(Icons.home),
        ),*/
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                  child: Stack(
                fit: StackFit.expand,
                children: [
                  AspectRatio(
                    aspectRatio: 1 / 1,
                    child: Image.asset(
                      'images/ryzen-detail.jpg',
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Flexible(
                        flex: 5,
                        fit: FlexFit.tight,
                        child: Container(
                          alignment: Alignment.topLeft,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: 10, top: 10),
                                  child: Container(
                                    width: 36,
                                    height: 36,
                                    child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        padding: EdgeInsets.all(1),
                                        color: Colors.white.withAlpha(800),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Icon(
                                          Icons.arrow_back,
                                          color: Colors.white,
                                        )),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 10, top: 10),
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 36,
                                        height: 36,
                                        child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            padding: EdgeInsets.all(1),
                                            color: Colors.white.withAlpha(800),
                                            onPressed: () {},
                                            child: Icon(
                                              Icons.favorite_border,
                                              color: Colors.white,
                                            )),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        width: 36,
                                        height: 36,
                                        child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            padding: EdgeInsets.all(1),
                                            color: Colors.white.withAlpha(800),
                                            onPressed: () {},
                                            child: Icon(
                                              Icons.contact_support_outlined,
                                              color: Colors.white,
                                            )),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 5,
                        child: Container(
                          child: Stack(
                            children: [
                              Positioned(
                                bottom: 10,
                                right: 10,
                                child: Container(
                                  width: 36,
                                  height: 36,
                                  child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      padding: EdgeInsets.all(1),
                                      color: Colors.white.withAlpha(800),
                                      onPressed: () {},
                                      child: Icon(
                                        Icons.link_sharp,
                                        color: Colors.white,
                                      )),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        child: Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Container(
                            child: Center(
                              child: Text(
                                'AMD Ryzen™ 5 3600X',
                                style: GoogleFonts.lato(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: appTheme.canvasColor,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(50),
                                    topLeft: Radius.circular(50))),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )),
              Flexible(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                decoration: UnderlineTabIndicator(
                                    borderSide: BorderSide(
                                        width: 0.2, color: Colors.black87)),
                                child: Padding(
                                  padding: EdgeInsets.only(bottom: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                          text: '\$999',
                                          style: GoogleFonts.lato(
                                              color: Colors.green,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 30),
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: '.99',
                                              style: GoogleFonts.lato(
                                                  color: Colors.green,
                                                  fontSize: 15),
                                            )
                                          ],
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            '129 Comments',
                                            style: GoogleFonts.lato(
                                                color: Colors.black38,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          RatingBarIndicator(
                                            //https://pub.dev/packages/flutter_rating_bar
                                            rating: 5,
                                            itemBuilder: (context, index) =>
                                                Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            itemCount: 5,
                                            itemSize: 20.0,
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          'About this item',
                                          style: GoogleFonts.lato(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          '❤  The world\'s most advanced processor in the desktop PC gaming segment',
                                          style: GoogleFonts.jura(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          '❤  Can deliver ultra-fast 100+ FPS performance in the world\'s most popular games',
                                          style: GoogleFonts.jura(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          '❤  6 Cores and 12 processing threads, bundled with the powerful AMD Wraith Spire cooler',
                                          style: GoogleFonts.jura(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          '❤  4.4 GHz Max Boost, unlocked for overclocking, 35 MB of game Cache, ddr-3200 support',
                                          style: GoogleFonts.jura(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          '❤  For the advanced socket AM4 platform, can support PCIe 4.0 on x570 motherboards',
                                          style: GoogleFonts.jura(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          '❤  Max Temps: 95°C',
                                          style: GoogleFonts.jura(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Text(
                                          '❤  With Wraith Spire cooler',
                                          style: GoogleFonts.jura(
                                              color: Colors.black,
                                              fontSize: 15),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          )),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              flex: 4,
                              fit: FlexFit.tight,
                              child: Padding(
                                padding: EdgeInsets.only(left: 20, right: 20),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 30,
                                          height: 30,
                                          child: RaisedButton(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              padding: EdgeInsets.all(1),
                                              color: Colors.grey,
                                              onPressed: () {},
                                              child: Icon(
                                                Icons.add,
                                                color: Colors.white,
                                              )),
                                        ),
                                        SizedBox(
                                          width: 40,
                                          height: 30,
                                          child: Center(
                                              child: Text(
                                            '01',
                                            style:
                                                GoogleFonts.abel(fontSize: 17),
                                          )),
                                        ),
                                        SizedBox(
                                          width: 30,
                                          height: 30,
                                          child: RaisedButton(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5)),
                                              padding: EdgeInsets.all(1),
                                              color: Colors.grey,
                                              onPressed: () {},
                                              child: Icon(
                                                Icons.remove,
                                                color: Colors.white,
                                              )),
                                        )
                                      ],
                                    ),
                                    RichText(
                                        text: TextSpan(
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20),
                                            text: 'Total: ',
                                            children: [
                                          TextSpan(text: '\$999.99')
                                        ]))
                                  ],
                                ),
                              ),
                            ),
                            Flexible(
                              flex: 5,
                              fit: FlexFit.tight,
                              child: Row(
                                children: [
                                  Spacer(
                                    flex: 1,
                                  ),
                                  Flexible(
                                    flex: 16,
                                    child: Padding(
                                      padding:
                                          EdgeInsets.only(top: 6, bottom: 6),
                                      child: SizedBox.expand(
                                        child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(36)),
                                          color: Colors.grey,
                                          onPressed: () {},
                                          child: Text(
                                            'Add to Bag',
                                            style: GoogleFonts.jura(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Spacer(
                                    flex: 1,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
